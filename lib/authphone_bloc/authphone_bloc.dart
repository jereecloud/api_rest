import 'dart:convert';
import 'package:apirest/api/api_services.dart';
import 'package:apirest/repositories/entities/user_entity.dart';
import 'package:apirest/repositories/models/user.dart';
import 'package:apirest/repositories/user_repository.dart';
import 'package:meta/meta.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:bloc/bloc.dart';
import 'package:flutter/services.dart';

import '../api/app_config.dart';
import 'bloc.dart';

class AuthphoneBloc extends Bloc<AuthphoneEvent, AuthphoneState> {
  ApiServices _apiServices = ApiServices();
  final UserRepository _userRepository;

  AuthphoneBloc({@required UserRepository userRepository})
      : assert(userRepository != null),
        _userRepository = userRepository;

  @override
  AuthphoneState get initialState => AuthphoneStateInitial();

  @override
  Stream<AuthphoneState> mapEventToState(AuthphoneEvent event) async* {
    if (event is AuthphoneSend) {
      yield* _mapAuthphoneSendToState(event);
    } else if (event is AuthphoneSendError) {
      yield AuthphoneStateInitial();
    } else if (event is AuthphoneValidate) {
      yield* _mapAuthphoneValidateToState(event);
    } else if (event is AuthphoneEditPhone) {
      yield AuthphoneStateInitial();
    } else if (event is AuthphoneValidateError) {
      yield AuthphoneStatePhone(event.phone);
    } else if (event is AuthphoneLoad) {
      yield* _mapAuthphoneLoad(event);
    }
  }

  Stream<AuthphoneState> _mapAuthphoneLoad(AuthphoneLoad event) async* {
    final shaPref = await SharedPreferences.getInstance();
    final token = await shaPref.get('token');
    if (token != null) {
      final user =
          User.fromEntity(UserEntity.fromJson(jsonDecode(shaPref.get('user'))));
      yield AuthphoneStateLogged(user);
    } else {
      yield AuthphoneStateNotLogged();
    }
  }

  Stream<AuthphoneState> _mapAuthphoneValidateToState(
      AuthphoneValidate event) async* {
    yield AuthphoneStateLoading(phone: event.phone, code: event.code);
    try {
      final response =
          await _apiServices.codeValidation(event.phone, event.code);
      if (response is User) {
        _userRepository.addUser(response);
        yield AuthphoneStateLogged(response);
      } else {
        yield AuthphoneStateError(response.body, event.phone);
      }
    } on PlatformException catch (e) {
      yield AuthphoneStateError("Error ${e.code}: ${e.message}", event.phone);
    }
  }

  Stream<AuthphoneState> _mapAuthphoneSendToState(AuthphoneSend event) async* {
    yield AuthphoneStateLoading(phone: event.phone);
    try {
//    final url = '${AppConfig.apiHost}/auth/user/phonevalidation';
//    final response = await http.post(
//      url,
//      body: event.phone.isEmpty ? {} : {"phone": "+54${event.phone}"},
//    );
      final response = await _apiServices.phoneValidation(event.phone);
      if (response.statusCode == 200) {
        yield AuthphoneStatePhone(event.phone);
      } else {
        yield AuthphoneStateError(jsonDecode(response.body)['message'], null);
      }
    } on PlatformException catch (e) {
      yield AuthphoneStateError("Error ${e.code}: ${e.message}", null);
    }
  }
}
