import 'package:equatable/equatable.dart';

abstract class AuthphoneEvent extends Equatable {
  @override
  List<Object> get props => [];
}

class AuthphoneLoad extends AuthphoneEvent {
  @override
  String toString() {
    return 'AuthphoneLoad{}';
  }
}

class AuthphoneSend extends AuthphoneEvent {
  final String phone;

  AuthphoneSend({this.phone});

  @override
  String toString() {
    return 'AuthphoneSend{phone: $phone}';
  }
}

class AuthphoneSendError extends AuthphoneEvent {
  @override
  String toString() {
    return 'AuthphoneSendError{}';
  }
}

class AuthphoneEditPhone extends AuthphoneEvent {
  @override
  String toString() {
    return 'AuthphoneEditPhone{}';
  }
}

class AuthphoneValidate extends AuthphoneEvent {
  final String phone;
  final String code;

  AuthphoneValidate({this.phone, this.code});

  @override
  String toString() {
    return 'AuthphoneValidate{phone: $phone, code: $code}';
  }
}

class AuthphoneValidateError extends AuthphoneEvent {
  final String phone;

  AuthphoneValidateError(this.phone);

  @override
  String toString() {
    return 'AuthphoneValidateError{phone: $phone}';
  }
}
