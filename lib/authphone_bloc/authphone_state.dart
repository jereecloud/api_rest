import 'package:apirest/repositories/models/user.dart';
import 'package:equatable/equatable.dart';

abstract class AuthphoneState extends Equatable {
  const AuthphoneState();

  @override
  List<Object> get props => [];
}

class AuthphoneStateInitial extends AuthphoneState {
  @override
  String toString() {
    return 'AuthphoneStateInitial{}';
  }
}

class AuthphoneStateNotLogged extends AuthphoneState {
  @override
  String toString() {
    return 'AuthphoneStateNotLogged{}';
  }
}

class AuthphoneStateLoading extends AuthphoneState {
  final String phone;
  final String code;

  AuthphoneStateLoading({this.phone, this.code});

  @override
  List<Object> get props => [phone, code];

  @override
  String toString() =>
      'AuthphoneStateLoading : { phone: $phone , code: $code }';
}

class AuthphoneStatePhone extends AuthphoneState {
  final String phone;

  AuthphoneStatePhone(this.phone);

  @override
  List<Object> get props => [phone];

  @override
  String toString() => 'AuthphoneStatePhone { phone: $phone }';
}

class AuthphoneStateError extends AuthphoneState {
  final String error;
  final String phone;

  AuthphoneStateError(this.error, this.phone);

  @override
  List<Object> get props => [error, phone];

  @override
  String toString() => 'AuthphoneStateError : { error: $error , phone: $phone}';
}

class AuthphoneStateCode extends AuthphoneState {
  final String phone;
  final String code;

  AuthphoneStateCode(this.phone, this.code);

  @override
  List<Object> get props => [phone, code];

  @override
  String toString() => 'AuthphoneStateCode : { phone: $phone , code: $code }';
}

class AuthphoneStateLogged extends AuthphoneState {
  final User user;

  AuthphoneStateLogged(this.user);

  @override
  String toString() {
    return 'AuthphoneStateLogged{user: $user}';
  }
}
