import 'package:apirest/repositories/entities/user_entity.dart';
import 'package:flutter/material.dart';

@immutable
class User {
  final int id;
  final String firstName;
  final String lastName;
  final String email;
  final String identification;
  final DateTime birthday;
  final String gender;
  final String phone;
  final String identificationBackImageURL;
  final String identificationFromImage;
  final String faceImageURL;
  final String firebaseToken;
  final String frindlyToken;
  final bool foreign;
  final bool renaper;
  final bool validatedEmail;
  final bool validatedDocumentation;
  final int idCurrentTrip;
  final bool active;
  final bool canDrive;
  final bool activeSubscription;
  final String avatar;
  final double kilometers;
  final double calories;
  final String monthlyTrips;
  final String monthlyCalories;
  final String monthlyKilometers;
  final String device;

  User({
    this.id,
    this.firstName,
    this.lastName,
    this.email,
    this.identification,
    this.birthday,
    this.gender,
    this.phone,
    this.identificationBackImageURL,
    this.identificationFromImage,
    this.faceImageURL,
    this.firebaseToken,
    this.frindlyToken,
    this.foreign,
    this.renaper,
    this.validatedEmail,
    this.validatedDocumentation,
    this.idCurrentTrip,
    this.active,
    this.canDrive,
    this.activeSubscription,
    this.avatar,
    this.kilometers,
    this.calories,
    this.monthlyTrips,
    this.monthlyCalories,
    this.monthlyKilometers,
    this.device,
  });

  User copyWith({
    int id,
    String firstName,
    String lastName,
    String email,
    String identification,
    DateTime birthday,
    String gender,
    String phone,
    String identificationBackImageURL,
    String identificationFromImage,
    String faceImageURL,
    String firebaseToken,
    String frindlyToken,
    bool foreign,
    bool renaper,
    bool validatedEmail,
    bool validatedDocumentation,
    int idCurrentTrip,
    bool active,
    bool canDrive,
    bool activeSubscription,
    String avatar,
    double kilometers,
    double calories,
    String monthlyTrips,
    String monthlyCalories,
    String monthlyKilometers,
    String device,
  }) {
    return User(
        id: id ?? this.id,
        firstName: firstName ?? this.firstName,
        lastName: lastName ?? this.lastName,
        email: email ?? this.email,
        identification: identification ?? this.identification,
        birthday: birthday ?? this.birthday,
        gender: gender ?? this.gender,
        phone: phone ?? this.phone,
        identificationBackImageURL:
            identificationBackImageURL ?? this.identificationBackImageURL,
        identificationFromImage:
            identificationFromImage ?? this.identificationFromImage,
        faceImageURL: faceImageURL ?? this.faceImageURL,
        firebaseToken: firebaseToken ?? this.firebaseToken,
        foreign: foreign ?? this.foreign,
        renaper: renaper ?? this.renaper,
        validatedEmail: validatedEmail ?? this.validatedEmail,
        validatedDocumentation:
            validatedDocumentation ?? this.validatedDocumentation,
        idCurrentTrip: idCurrentTrip ?? this.idCurrentTrip,
        active: active ?? this.active,
        canDrive: canDrive ?? this.canDrive,
        activeSubscription: activeSubscription ?? this.activeSubscription,
        avatar: avatar ?? this.avatar,
        kilometers: kilometers ?? this.kilometers,
        calories: calories ?? this.calories,
        monthlyTrips: monthlyTrips ?? this.monthlyTrips,
        monthlyCalories: monthlyCalories ?? this.monthlyCalories,
        monthlyKilometers: monthlyKilometers ?? this.monthlyKilometers,
        device: device ?? this.device);
  }

  @override
  String toString() {
    return 'User{id: $id, firstName: $firstName, lastName: $lastName, email: $email, identification: $identification, birthday: $birthday, gender: $gender, phone: $phone, identificationBackImageURL: $identificationBackImageURL, identificationFromImage: $identificationFromImage, faceImageURL: $faceImageURL, firebaseToken: $firebaseToken, frindlyToken: $frindlyToken, foreign: $foreign, renaper: $renaper, validatedEmail: $validatedEmail, validatedDocumentation: $validatedDocumentation, idCurrentTrip: $idCurrentTrip, active: $active, canDrive: $canDrive, activeSubscription: $activeSubscription, avatar: $avatar, kilometers: $kilometers, calories: $calories, monthlyTrips: $monthlyTrips, monthlyCalories: $monthlyCalories, monthlyKilometers: $monthlyKilometers, device: $device}';
  }

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is User &&
          runtimeType == other.runtimeType &&
          id == other.id &&
          firstName == other.firstName &&
          lastName == other.lastName &&
          email == other.email &&
          identification == other.identification &&
          birthday == other.birthday &&
          gender == other.gender &&
          phone == other.phone &&
          identificationBackImageURL == other.identificationBackImageURL &&
          identificationFromImage == other.identificationFromImage &&
          faceImageURL == other.faceImageURL &&
          firebaseToken == other.firebaseToken &&
          frindlyToken == other.frindlyToken &&
          foreign == other.foreign &&
          renaper == other.renaper &&
          validatedEmail == other.validatedEmail &&
          validatedDocumentation == other.validatedDocumentation &&
          idCurrentTrip == other.idCurrentTrip &&
          active == other.active &&
          canDrive == other.canDrive &&
          activeSubscription == other.activeSubscription &&
          avatar == other.avatar &&
          kilometers == other.kilometers &&
          calories == other.calories &&
          monthlyTrips == other.monthlyTrips &&
          monthlyCalories == other.monthlyCalories &&
          monthlyKilometers == other.monthlyKilometers &&
          device == other.device;

  @override
  int get hashCode =>
      id.hashCode ^
      firstName.hashCode ^
      lastName.hashCode ^
      email.hashCode ^
      identification.hashCode ^
      birthday.hashCode ^
      gender.hashCode ^
      phone.hashCode ^
      identificationBackImageURL.hashCode ^
      identificationFromImage.hashCode ^
      faceImageURL.hashCode ^
      firebaseToken.hashCode ^
      frindlyToken.hashCode ^
      foreign.hashCode ^
      renaper.hashCode ^
      validatedEmail.hashCode ^
      validatedDocumentation.hashCode ^
      idCurrentTrip.hashCode ^
      active.hashCode ^
      canDrive.hashCode ^
      activeSubscription.hashCode ^
      avatar.hashCode ^
      kilometers.hashCode ^
      calories.hashCode ^
      monthlyTrips.hashCode ^
      monthlyCalories.hashCode ^
      monthlyKilometers.hashCode ^
      device.hashCode;

  UserEntity toEntity() {
    return UserEntity(
      id,
      firstName,
      lastName,
      email,
      identification,
      birthday,
      gender,
      phone,
      identificationBackImageURL,
      identificationFromImage,
      faceImageURL,
      firebaseToken,
      frindlyToken,
      foreign,
      renaper,
      validatedEmail,
      validatedDocumentation,
      idCurrentTrip,
      active,
      canDrive,
      activeSubscription,
      avatar,
      kilometers,
      calories,
      monthlyTrips,
      monthlyCalories,
      monthlyKilometers,
      device,
    );
  }

  static User fromEntity(UserEntity entity) {
    return User(
      id: entity.id,
      firstName: entity.firstName,
      lastName: entity.lastName,
      email: entity.email,
      identification: entity.identification,
      birthday: entity.birthday,
      gender: entity.gender,
      phone: entity.phone,
      identificationBackImageURL: entity.identificationBackImageURL,
      identificationFromImage: entity.identificationFromImage,
      active: entity.active,
      activeSubscription: entity.activeSubscription,
      avatar: entity.avatar,
      calories: entity.calories,
      canDrive: entity.canDrive,
      device: entity.device,
      faceImageURL: entity.faceImageURL,
      firebaseToken: entity.firebaseToken,
      foreign: entity.foreign,
      frindlyToken: entity.frindlyToken,
      idCurrentTrip: entity.idCurrentTrip,
      kilometers: entity.kilometers,
      monthlyCalories: entity.monthlyCalories,
      monthlyKilometers: entity.monthlyKilometers,
      monthlyTrips: entity.monthlyTrips,
      renaper: entity.renaper,
      validatedDocumentation: entity.validatedDocumentation,
      validatedEmail: entity.validatedEmail,
    );
  }
}
