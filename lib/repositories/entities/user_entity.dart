import 'dart:core';
import 'dart:core';

import 'package:equatable/equatable.dart';
import 'package:string_validator/string_validator.dart';

class UserEntity extends Equatable {
  final int id;
  final String firstName;
  final String lastName;
  final String email;
  final String identification;
  final DateTime birthday;
  final String gender;
  final String phone;
  final String identificationBackImageURL;
  final String identificationFromImage;
  final String faceImageURL;
  final String firebaseToken;
  final String frindlyToken;
  final bool foreign;
  final bool renaper;
  final bool validatedEmail;
  final bool validatedDocumentation;
  final int idCurrentTrip;
  final bool active;
  final bool canDrive;
  final bool activeSubscription;
  final String avatar;
  final double kilometers;
  final double calories;
  final String monthlyTrips;
  final String monthlyCalories;
  final String monthlyKilometers;
  final String device;

  //TODO Hacer las clases de los atributos opcionales.
//  final Trip trips;
//  final Position position;
//  final Trip currentTrip;
//  final UserStatus userStatuses;
//  final UserStatus latestStatus;
//  final Subscription subscriptions;
//  final Subscription currentSubscription;

  UserEntity(
    this.id,
    this.firstName,
    this.lastName,
    this.email,
    this.identification,
    this.birthday,
    this.gender,
    this.phone,
    this.identificationBackImageURL,
    this.identificationFromImage,
    this.faceImageURL,
    this.firebaseToken,
    this.frindlyToken,
    this.foreign,
    this.renaper,
    this.validatedEmail,
    this.validatedDocumentation,
    this.idCurrentTrip,
    this.active,
    this.canDrive,
    this.activeSubscription,
    this.avatar,
    this.kilometers,
    this.calories,
    this.monthlyTrips,
    this.monthlyCalories,
    this.monthlyKilometers,
    this.device,
  );

  Map<String, Object> toJson() {
    return {
      'id': this.id,
      'firstName': this.firstName,
      'lastName': this.lastName,
      'email': this.email,
      'identification': this.identification,
      'birthday': this.birthday,
      'gender': this.gender,
      'phone': this.phone,
      'identificationBackImageURL': this.identificationBackImageURL,
      'identificationFromImage': this.identificationFromImage,
      'faceImageURL': this.faceImageURL,
      'firebaseToken': this.firebaseToken,
      'frindlyToken': this.frindlyToken,
      'foreign': this.foreign,
      'renaper': this.renaper,
      'validatedEmail': this.validatedEmail,
      'validatedDocumentation': this.validatedDocumentation,
      'idCurrentTrip': this.idCurrentTrip,
      'active': this.active,
      'canDrive': this.canDrive,
      'activeSubscription': this.activeSubscription,
      'avatar': this.avatar,
      'kilometers': this.kilometers,
      'calories': this.calories,
      'monthlyTrips': this.monthlyTrips,
      'monthlyCalories': this.monthlyCalories,
      'monthlyKilometers': this.monthlyKilometers,
      'device': this.device
    };
  }

  Map<String, dynamic> toMap() {
    return {
      'id': this.id,
      'firstName': this.firstName,
      'lastName': this.lastName,
      'email': this.email,
      'identification': this.identification,
      'birthday': this.birthday,
      'gender': this.gender,
      'phone': this.phone,
      'identificationBackImageURL': this.identificationBackImageURL,
      'identificationFromImage': this.identificationFromImage,
      'faceImageURL': this.faceImageURL,
      'firebaseToken': this.firebaseToken,
      'frindlyToken': this.frindlyToken,
      'foreign': this.foreign,
      'renaper': this.renaper,
      'validatedEmail': this.validatedEmail,
      'validatedDocumentation': this.validatedDocumentation,
      'idCurrentTrip': this.idCurrentTrip,
      'active': this.active,
      'canDrive': this.canDrive,
      'activeSubscription': this.activeSubscription,
      'avatar': this.avatar,
      'kilometers': this.kilometers,
      'calories': this.calories,
      'monthlyTrips': this.monthlyTrips,
      'monthlyCalories': this.monthlyCalories,
      'monthlyKilometers': this.monthlyKilometers,
      'device': this.device
    };
  }

  @override
  String toString() {
    return 'UserEntity{id: $id, firstName: $firstName, lastName: $lastName, email: $email, identification: $identification, birthday: $birthday, gender: $gender, phone: $phone, identificationBackImageURL: $identificationBackImageURL, identificationFromImage: $identificationFromImage, faceImageURL: $faceImageURL, firebaseToken: $firebaseToken, frindlyToken: $frindlyToken, foreign: $foreign, renaper: $renaper, validatedEmail: $validatedEmail, validatedDocumentation: $validatedDocumentation, idCurrentTrip: $idCurrentTrip, active: $active, canDrive: $canDrive, activeSubscription: $activeSubscription, avatar: $avatar, kilometers: $kilometers, calories: $calories, monthlyTrips: $monthlyTrips, monthlyCalories: $monthlyCalories, monthlyKilometers: $monthlyKilometers, device: $device}';
  }

  UserEntity.fromMap(Map<dynamic, dynamic> data)
      : id = data['id'],
        firstName = data['firstName'],
        lastName = data['lastName'],
        email = data['email'],
        identification = data['identification'],
        birthday = data['birthday'],
        gender = data['gender'],
        phone = data['phone'],
        identificationBackImageURL = data['identificationBackImageURL'],
        identificationFromImage = data['identificationFromImage'],
        faceImageURL = data['faceImageURL'],
        firebaseToken = data['firebaseToken'],
        frindlyToken = data['frindlyToken'],
        foreign = data['foreign'],
        renaper = data['renaper'],
        validatedEmail = data['validatedEmail'],
        validatedDocumentation = data['validatedDocumentation'],
        idCurrentTrip = data['idCurrentTrip'],
        active = data['active'],
        canDrive = data['canDrive'],
        activeSubscription = data['activeSubscription'],
        avatar = data['avatar'],
        kilometers = toFloat(data['kilometers'].toString()),
        calories = toFloat(data['calories'].toString()),
        monthlyTrips = data['monthlyTrips'],
        monthlyCalories = data['monthlyCalories'],
        monthlyKilometers = data['monthlyKilometers'],
        device = data['device'];

  UserEntity.fromJson(Map<String, dynamic> data)
      : id = data['id'],
        firstName = data['firstName'],
        lastName = data['lastName'],
        email = data['email'],
        identification = data['identification'],
        birthday = data['birthday'],
        gender = data['gender'],
        phone = data['phone'],
        identificationBackImageURL = data['identificationBackImageURL'],
        identificationFromImage = data['identificationFromImage'],
        faceImageURL = data['faceImageURL'],
        firebaseToken = data['firebaseToken'],
        frindlyToken = data['frindlyToken'],
        foreign = data['foreign'],
        renaper = data['renaper'],
        validatedEmail = data['validatedEmail'],
        validatedDocumentation = data['validatedDocumentation'],
        idCurrentTrip = data['idCurrentTrip'],
        active = data['active'],
        canDrive = data['canDrive'],
        activeSubscription = data['activeSubscription'],
        avatar = data['avatar'],
        kilometers = toFloat(data['kilometers'].toString()),
        calories = toFloat(data['calories'].toString()),
        monthlyTrips = data['monthlyTrips'],
        monthlyCalories = data['monthlyCalories'],
        monthlyKilometers = data['monthlyKilometers'],
        device = data['device'];

  @override
  List<Object> get props => [
        this.id,
        this.firstName,
        this.lastName,
        this.email,
        this.identification,
        this.birthday,
        this.gender,
        this.phone,
        this.identificationBackImageURL,
        this.identificationFromImage,
        this.faceImageURL,
        this.firebaseToken,
        this.frindlyToken,
        this.foreign,
        this.renaper,
        this.validatedEmail,
        this.validatedDocumentation,
        this.idCurrentTrip,
        this.active,
        this.canDrive,
        this.activeSubscription,
        this.avatar,
        this.kilometers,
        this.calories,
        this.monthlyTrips,
        this.monthlyCalories,
        this.monthlyKilometers,
        this.device,
      ];
}
