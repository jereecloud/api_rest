import 'package:apirest/authphone_bloc/authphone_bloc.dart';
import 'package:apirest/repositories/user_repository.dart';
import 'package:apirest/screens/code_screen.dart';
import 'package:apirest/screens/error_screen.dart';
import 'package:apirest/screens/splash_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'authphone_bloc/authphone_event.dart';
import 'screens/home_screen.dart';
import 'screens/phone_screen.dart';
import 'simple_bloc_delegate.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  BlocSupervisor.delegate = SimpleBlocDelegate();
  runApp(BlocProvider(
      create: (context) => AuthphoneBloc(userRepository: UserRepository()),
      child: MyApp()));
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'API Rest',
      debugShowCheckedModeBanner: false,
      home: SplashScreen(),
      routes: {
        HomeScreen.id: (context) => HomeScreen(),
        PhoneScreen.id: (context) => PhoneScreen(),
        CodeScreen.id: (context) => CodeScreen(),
        ErrorScreen.id: (context) => ErrorScreen(),
        SplashScreen.id: (context) => SplashScreen(),
      },
    );
  }
}
