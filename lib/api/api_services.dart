import 'dart:convert';

import 'package:apirest/repositories/entities/user_entity.dart';
import 'package:apirest/repositories/models/user.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

import 'app_config.dart';

class ApiServices {
  Future<dynamic> phoneValidation(String phone) async {
    try {
      final url = '${AppConfig.apiHost}/auth/user/phonevalidation';
      final response = await http.post(
        url,
        body: phone.isEmpty ? {} : {"phone": "+54$phone"},
      );
      return response;
    } on Exception catch (e) {
      throw e;
    }
  }

  Future<dynamic> codeValidation(String phone, String code) async {
    try {
      final url = '${AppConfig.apiHost}/auth/user/codevalidation';
      final response = await http.post(
        url,
        body: {"phone": "+54$phone", 'code': '$code'},
      );

      if (response.statusCode == 200) {
        final bodyParsed = jsonDecode(response.body);
        final token = bodyParsed['data']['token'];
//        print('token: $token');
        final sharedPref = await SharedPreferences.getInstance();
        sharedPref.setString('token', token);
//        print('token:' + sharedPref.get('token').toString());
        User user =
            User.fromEntity(UserEntity.fromJson(bodyParsed['data']['user']));
//        print(user.toEntity());
//        print(user.toEntity().toJson());
        sharedPref.setString('user', jsonEncode(user.toEntity().toJson()));
//        print(sharedPref.getString('user'));
        return user;
      } else {
        return response;
      }
    } on Exception catch (e) {
      throw e;
    }
  }
}
