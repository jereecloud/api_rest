import 'package:apirest/authphone_bloc/authphone_bloc.dart';
import 'package:apirest/authphone_bloc/authphone_state.dart';
import 'package:apirest/authphone_bloc/bloc.dart';
import 'package:apirest/screens/home_screen.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class CodeScreen extends StatefulWidget {
  static String id = 'code_screen';
  final String phone;

  const CodeScreen({Key key, this.phone = '3454120983'}) : super(key: key);

  @override
  _CodeScreenState createState() => _CodeScreenState();
}

class _CodeScreenState extends State<CodeScreen> {
  final TextEditingController _codeController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return BlocListener<AuthphoneBloc, AuthphoneState>(
      listener: (context, state) {
        if (state is AuthphoneStateCode) {
          Navigator.pushNamed(context, HomeScreen.id);
        }
        if (state is AuthphoneStateLogged) {
          Navigator.pushNamed(context, HomeScreen.id);
        }
      },
      child: BlocBuilder<AuthphoneBloc, AuthphoneState>(
        builder: (context, state) {
          return Scaffold(
            body: state is AuthphoneStateLoading
                ? loadingIndicator()
                : _codeForm(),
          );
        },
      ),
    );
  }

  Widget _codeForm() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Text(
          'Telefono',
          style: TextStyle(
              fontSize: 20.0,
              fontWeight: FontWeight.bold,
              color: Colors.blueGrey),
        ),
        ListTile(
          leading: Icon(Icons.phone),
          trailing: Text(
            '+54${widget.phone}',
            style: TextStyle(
                fontSize: 18.0,
                fontWeight: FontWeight.bold,
                color: Colors.blueGrey),
          ),
        ),
        Text(
          'Código',
          style: TextStyle(
              fontSize: 20.0,
              fontWeight: FontWeight.bold,
              color: Colors.lightBlueAccent),
        ),
        Container(
          padding: EdgeInsets.symmetric(vertical: 10.0, horizontal: 20.0),
          child: TextField(
            textAlign: TextAlign.center,
            controller: _codeController,
            keyboardType: TextInputType.phone,
          ),
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            RaisedButton(
              color: Colors.redAccent,
              child: Text(
                'Reenviar',
                style: TextStyle(
                  fontSize: 20.0,
                  color: Colors.white,
                ),
              ),
              onPressed: () {
                BlocProvider.of<AuthphoneBloc>(context)
                  ..add(AuthphoneSend(phone: widget.phone));
              },
            ),
            RaisedButton(
              color: Colors.green,
              child: Text(
                'Validar',
                style: TextStyle(
                  fontSize: 20.0,
                  color: Colors.white,
                ),
              ),
              onPressed: () {
                BlocProvider.of<AuthphoneBloc>(context)
                  ..add(AuthphoneValidate(
                      code: _codeController.text, phone: widget.phone));
              },
            ),
          ],
        ),
        Padding(
          padding: EdgeInsets.only(top: 30.0),
        ),
        RaisedButton(
          color: Colors.yellow,
          onPressed: () {
            BlocProvider.of<AuthphoneBloc>(context)..add(AuthphoneEditPhone());
            Navigator.pop(context);
          },
          child: Text(
            'Volver',
            style: TextStyle(fontSize: 20.0),
          ),
        )
      ],
    );
  }

  Widget loadingIndicator() {
    return Center(
      child: CircularProgressIndicator(),
    );
  }
}
