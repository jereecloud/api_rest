import 'package:apirest/authphone_bloc/authphone_state.dart';
import 'package:apirest/authphone_bloc/bloc.dart';
import 'package:apirest/screens/home_screen.dart';
import 'package:apirest/screens/phone_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SplashScreen extends StatefulWidget {
  static String id = 'splash_screen';

  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  void initState() {
    super.initState();
//    BlocProvider.of<AuthphoneBloc>(context)..add(AuthphoneLoad());
//    this.check();
  }

//  check() async {
//    final sharedPref = await SharedPreferences.getInstance();
//    final token = sharedPref.getString('token');
//    print(token);
//    if (token == null) {
//
//    } else {
//
//    }
//  }

  @override
  Widget build(BuildContext context) {
    BlocProvider.of<AuthphoneBloc>(context)..add(AuthphoneLoad());

    return BlocListener<AuthphoneBloc, AuthphoneState>(
      listener: (context, state) {
        if (state is AuthphoneStateLogged) {
          Navigator.pushReplacementNamed(context, HomeScreen.id);
        } else {
          Navigator.pushReplacementNamed(context, PhoneScreen.id);
        }
      },
      child: Scaffold(
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              Hero(
                  tag: 'logo',
                  child: FlutterLogo(
                    size: 200.0,
                  )),
              CircularProgressIndicator(),
              Text(
                'Splash Screen',
                style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 25,
                    color: Colors.lightBlueAccent),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
