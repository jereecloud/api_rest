import 'package:apirest/authphone_bloc/authphone_bloc.dart';
import 'package:apirest/authphone_bloc/authphone_state.dart';
import 'package:apirest/authphone_bloc/bloc.dart';
import 'package:apirest/screens/code_screen.dart';
import 'package:apirest/screens/error_screen.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class PhoneScreen extends StatefulWidget {
  static String id = 'login_screen';

  @override
  _PhoneScreenState createState() => _PhoneScreenState();
}

class _PhoneScreenState extends State<PhoneScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: LoginForm(),
    );
  }
}

class LoginForm extends StatefulWidget {
  @override
  _LoginFormState createState() => _LoginFormState();
}

class _LoginFormState extends State<LoginForm> {
  TextEditingController _phoneController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return BlocListener<AuthphoneBloc, AuthphoneState>(
      listener: (context, state) {
        if (state is AuthphoneStateError) {
          Navigator.pushNamed(context, ErrorScreen.id);
        }
        if (state is AuthphoneStatePhone) {
          Navigator.pushNamed(context, CodeScreen.id,
              arguments: {'phone': _phoneController.text});
        }
      },
      child:
          BlocBuilder<AuthphoneBloc, AuthphoneState>(builder: (context, state) {
        return state is AuthphoneStateInitial ||
                state is AuthphoneStateNotLogged
            ? Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text(
                      'Telefono',
                      style: TextStyle(
                          fontSize: 20.0,
                          fontWeight: FontWeight.bold,
                          color: Colors.lightBlueAccent),
                    ),
                    Container(
                      padding: EdgeInsets.symmetric(
                          vertical: 10.0, horizontal: 20.0),
                      child: TextField(
                        textAlign: TextAlign.center,
                        controller: _phoneController,
                        keyboardType: TextInputType.phone,
                      ),
                    ),
                    RaisedButton(
                      color: Colors.redAccent,
                      child: Text(
                        'Enviar',
                        style: TextStyle(
                          fontSize: 20.0,
                          color: Colors.white,
                        ),
                      ),
                      onPressed: () {
                        BlocProvider.of<AuthphoneBloc>(context)
                          ..add(AuthphoneSend(phone: _phoneController.text));
                      },
                    ),
                  ],
                ),
              )
            : loadingIndicator();
      }),
    );
  }

  Widget loadingIndicator() {
    return Center(
      child: CircularProgressIndicator(),
    );
  }
}
