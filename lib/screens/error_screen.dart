import 'package:apirest/authphone_bloc/bloc.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class ErrorScreen extends StatelessWidget {
  static String id = 'error_screen';

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<AuthphoneBloc, AuthphoneState>(
        builder: (context, state) {
      return Scaffold(
        body: SafeArea(
          child: Center(
            child: state is AuthphoneStateError
                ? Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Text(
                        'Error: '
                        '${state.error}',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            fontSize: 20.0,
                            fontWeight: FontWeight.bold,
                            color: Colors.red),
                      ),
                      RaisedButton(
                        color: Colors.yellow,
                        onPressed: () {
                          BlocProvider.of<AuthphoneBloc>(context)
                            ..add(state.phone == null
                                ? AuthphoneSendError()
                                : AuthphoneValidateError(state.phone));
                          Navigator.pop(context);
                        },
                        child: Text(
                          'Volver',
                          style: TextStyle(fontSize: 20.0),
                        ),
                      )
                    ],
                  )
                : Container(height: 0.0, width: 0.0),
          ),
        ),
      );
    });
  }
}
